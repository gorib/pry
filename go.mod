module gitlab.com/gorib/pry

go 1.21.0

require (
	github.com/getsentry/sentry-go v0.31.1
	github.com/rs/zerolog v1.33.0
	gitlab.com/gorib/session v0.0.0-20250224141908-df66ea5a990f
)

require (
	github.com/google/go-cmp v0.7.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
)
